ARG version=latest
FROM docker.io/lavasoftware/lava-dispatcher:${version}
WORKDIR /opt/
RUN set -ex; \
    apt-get -q update; \
    DEBIAN_FRONTEND=noninteractive apt-get -q -y install build-essential git pkg-config cmake libusb-dev libftdi-dev

RUN set -ex; \
    git clone https://github.com/96boards/96boards-uart.git; \
    # Avoid using new libusb because of build-error
    cd 96boards-uart; \
    git checkout 1d2bc993083d97b54d21ecdf72556066efce11f7; \
    cd 96boardsctl/; \
    cmake .; \
    make

FROM docker.io/lavasoftware/lava-dispatcher:${version}

ARG extra_packages=""

RUN set -ex; \
    sed -i '/^Components:/ s/$/ contrib non-free non-free-firmware/' /etc/apt/sources.list.d/debian.sources; \
    apt-get -q update; \
    DEBIAN_FRONTEND=noninteractive apt-get -q -y install ${extra_packages} libftdi1 net-tools snmp snmp-mibs-downloader; \
    download-mibs; \
    mkdir -p /usr/share/snmp/mibs/; \
    mkdir -p /usr/local/lab-scripts/

# Add MIBs
ADD powernet428.mib /usr/share/snmp/mibs/

# Add lab scripts
ADD https://git.linaro.org/lava/lava-lab.git/plain/shared/lab-scripts/snmp_pdu_control /usr/local/lab-scripts/
ADD https://git.linaro.org/lava/lava-lab.git/plain/shared/lab-scripts/eth008_control /usr/local/lab-scripts/
RUN chmod a+x /usr/local/lab-scripts/snmp_pdu_control /usr/local/lab-scripts/eth008_control

# Add 96boardsctl
COPY --from=0 /opt/96boards-uart/96boardsctl/96boardsctl /usr/bin/

ARG server=lava-server

# Old config file ( < 2021)
RUN set -ex; \
    echo "MASTER_URL=\"tcp://${server}:5556\"" >> /etc/lava-dispatcher/lava-slave; \
    echo "LOGGER_URL=\"tcp://${server}:5555\"" >> /etc/lava-dispatcher/lava-slave; \
    echo "URL=\"http://${server}/\"" > /etc/lava-dispatcher/lava-worker

ENTRYPOINT ["/root/entrypoint.sh"]
